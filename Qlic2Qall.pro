QT         += network
unix:LIBS  += -lldap
win32:LIBS += -lopenldap

TARGET = Qlic2Qall
TEMPLATE = app

MOC_DIR = ./build/moc

OBJECTS_DIR = ./build

UI_DIR = ./build/ui

UI_HEADERS_DIR = ./build/ui

SOURCES += src/main.cpp \
           src/mainwindow.cpp \
           src/logindialog.cpp \
           src/contactlist.cpp \
           src/configurationdialog.cpp \
           src/qtfilechooser.cpp

HEADERS += src/mainwindow.h \
           src/logindialog.h \
           src/contactlist.h \
           src/configurationdialog.h \
           src/qtfilechooser.h

FORMS += ui/mainwindow.ui \
         ui/logindialog.ui \
         ui/configurationdialog.ui
