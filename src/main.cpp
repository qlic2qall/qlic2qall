#include <QtGui/QApplication>
#include "logindialog.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setOrganizationName("RedKite");
    QApplication::setApplicationName("Qlic2Qall");
    LoginDialog ld;
    MainWindow w;
    QTcpSocket socket;

    // Connection dialog
    ld.setSocket(&socket);
    if (ld.exec() == QDialog::Rejected)
        return 0;
    socket.disconnect();

    // Main application window
    w.setSocket(&socket);
    w.show();
    return a.exec();
}


/************************************** TODO List ***********************************************\
 *
 * - Add the possibility to choose a prefered number (which will be automatically selected)
 * - Search with startWith on words and with contains on numbers (in searchLE)
 * - Autocomplete words if there is only one possibilities for the x next letters (in searchLE)
 * - Make a more serious check of the number in ContactList::isValidNumber()
 * - call formatNumber when we are calling a number writen in the line edit
 *
\************************************************************************************************/
