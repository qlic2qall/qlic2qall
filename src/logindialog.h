#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include "ui_logindialog.h"
#include <QTcpSocket>
#include <QSettings>
#include <QVariant>
#include <QRegExp>
#include <QMessageBox>
#include <QString>

class LoginDialog : public QDialog, public Ui::LoginDialog
{
    Q_OBJECT

public:
    LoginDialog(QWidget *parent = 0);

    void setSocket(QTcpSocket *);

private slots:
    void telnetConnected();
    void on_connectPB_clicked();

private:
    //QtTelnet *telnet;
    QTcpSocket *socket;
};

#endif // LOGINDIALOG_H
