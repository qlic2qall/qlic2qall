#ifndef QTFILECHOOSER_H
#define QTFILECHOOSER_H

#include <QWidget>

class QLineEdit;
class QPushButton;

class QtFileChooser : public QWidget
{
    Q_OBJECT

public:
    explicit QtFileChooser(QWidget *parent=0);
    ~QtFileChooser();

    QString text() const;
    void setText(const QString &text);

    QString fileFilter() const;
    void setFileFilter(const QString &fileFilter);

    void setOpenFolder() { m_openFolder = true; }

public slots:
    void updateBackgroundColor();

protected:
    bool eventFilter(QObject *obj, QEvent *event);

private slots:
    void slotChooseFile();

private:
    QLineEdit   *m_lineEdit;
    QPushButton *m_pushButton;
    QString      m_fileFilter;
    QString      m_openPath;
    bool         m_openFolder;  // TODO use enum
};

#endif // QTFILECHOOSER_H
