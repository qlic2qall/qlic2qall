#include "configurationdialog.h"
#include "ui_configurationdialog.h"

#include <QSettings>

ConfigurationDialog::ConfigurationDialog(QWidget *parent) : QDialog(parent), ui(new Ui::ConfigurationDialog)
{
    ui->setupUi(this);

    QSettings settings;

    ui->CSVfileCB->setChecked(settings.value("config/useCSV", false).toBool());
    ui->CSVfilePathLE->setText(settings.value("config/CSVfilePath", "").toString());

    ui->LDAPserverCB->setChecked(settings.value("config/useLDAP", false).toBool());
    ui->LDAPaddressLE->setText(settings.value("config/LDAPaddress", "").toString());
    ui->LDAPbaseLE->setText(settings.value("config/LDAPbase", "").toString());
}

ConfigurationDialog::~ConfigurationDialog()
{
    delete ui;
}

void ConfigurationDialog::accept()
{
    QSettings settings;

    settings.setValue("config/useCSV", ui->CSVfileCB->isChecked());
    settings.setValue("config/CSVfilePath", ui->CSVfilePathLE->text());

    settings.setValue("config/useLDAP", ui->LDAPserverCB->isChecked());
    settings.setValue("config/LDAPaddress", ui->LDAPaddressLE->text());
    settings.setValue("config/LDAPbase", ui->LDAPbaseLE->text());

    QDialog::accept();
}
