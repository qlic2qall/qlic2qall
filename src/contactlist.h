#ifndef CONTACTLIST_H
#define CONTACTLIST_H

#include <QApplication>
#include <QWidget>
#include <QListWidget>
#include <QListWidgetItem>
#include <QLineEdit>
#include <QFile>
#include <QTextStream>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QList>
#include <QStringList>
#include <QPushButton>
#include <QIcon>
#include <QKeyEvent>
#include <QRegExp>
#include <QPalette>

#define NAME 0
#define SURNAME 1
#define DETAILS 2
#define HOME 3
#define WORK 4
#define MOBILE 5
#define FAX 6
#define INTERNAL 7

//#define NBR_OF_NUMBER 5
//#define PHONE_NUMBERS 34567
#define NATIONAL_PREFIXE "0032"

#define LIST_ITEM_HEIGHT 25

class Contact
{    
    static int counter;

public:
    Contact(const QStringList &);
    ~Contact();

    int getId() const;
    const QStringList & getSL() const;
    const QString & getDisplayName() const;
    void setDisplayName(const QString &);
    QListWidgetItem * getItem() const;
    const QList<QListWidgetItem *> & getNumberItems() const;

    static QString formatNumber(const QString &);

private:
    int id;
    QStringList stringList;
    QString displayName;
    QIcon icon;
    QListWidgetItem *item;
    QList<QListWidgetItem *> numbers;
};

class ContactList : public QWidget
{
    Q_OBJECT

public:
    ContactList(QWidget *parent);
    ~ContactList();

    bool loadContacts(const QString &path);
    bool loadContacts(const QString &address, const QString &base);
    void clearContacts();
    Contact * findContact(QListWidgetItem * const) const;
    bool isValidNumber(QString);
    QString nameFromNumber(const QString &number) const;

signals:
    void userSeleced(QString displayName);
    void numberSelected(QString number);
    void callAsked(QString number);

protected:
    bool eventFilter(QObject *obj, QEvent *ev);

private slots:
    void filterList(const QString &);
    void contactSelected(QListWidgetItem*);
    void callPB_clicked();

private:
    enum EState {
        selectingUser,
        selectingNumber
    };
    EState state;

    QListWidget *contactList;
    QLineEdit *searchLE;
    QPushButton *callPB;
    QList<Contact *> contacts;
};

#endif // CONTACTLIST_H
