#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QQueue>
#include <QSystemTrayIcon>

namespace Ui
{
    class MainWindow;
}

class ContactList;
class QTcpSocket;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setSocket(QTcpSocket *);

public slots:
    void initiateCall(QString number);
    void treatIncomingData();
    void showHide();
    void trayActivated(QSystemTrayIcon::ActivationReason);
    void timerSlot(); // to avoid double popup due to the double ringing when both line are free
    void slotConfigureDialog();
    void loadContacts();

protected:
    void closeEvent(QCloseEvent *event);

private:
    void buildTrayMenu();

    bool connectionEnable;
    Ui::MainWindow *ui;
    QTcpSocket *socket;
    ContactList *m_contactList;
    QSystemTrayIcon *tray;
    QMenu *trayMenu;
    QQueue<QString> callerID;
};

#endif // MAINWINDOW_H
