#include "logindialog.h"

LoginDialog::LoginDialog(QWidget *parent) : QDialog(parent)
{
    QSettings settings;
    setupUi(this);
    moreW->setVisible(false);
    this->setFixedWidth(1000);

    this->setWindowIcon(QIcon("./images/linphone.png"));

    usernameLE->setText(settings.value("login/username", "").toString());
    ipLE->setText(settings.value("login/ip", QString("127.0.0.1")).toString());
    portSB->setValue(settings.value("login/port", 5038).toInt());
}

void LoginDialog::telnetConnected()
{
    QString username = usernameLE->text(), password = passwordLE->text(), ip = ipLE->text();

    socket->write("Action: login\r\n");
    socket->write(QString("Username: %1\r\n").arg(username).toLatin1());
    socket->write(QString("Secret: %1\r\n").arg(password).toLatin1());
    socket->write("\r\n");

    // If the login is accepted by asterix, we can accept
    this->accept();
}

void LoginDialog::setSocket(QTcpSocket *sock)
{
    socket = sock;

    connect(socket, SIGNAL(connected()), this, SLOT(telnetConnected()));
}

void LoginDialog::on_connectPB_clicked()
{
    QSettings settings;
    QString username = usernameLE->text(), password = passwordLE->text(), ip = ipLE->text();
    int port = portSB->value();

    // Save login, ip, port for the next time (Add a case 'Save password')
    settings.setValue("login/username", username);
    settings.setValue("login/ip", ip);
    settings.setValue("login/port", port);

    // Check IP validity
    QRegExp regIP("(\\d?\\d?\\d)\\.(\\d?\\d?\\d)\\.(\\d?\\d?\\d)\\.(\\d?\\d?\\d)");

    if ( !regIP.exactMatch(ip)
        || regIP.cap(1).toInt()>255
        || regIP.cap(2).toInt()>255
        || regIP.cap(3).toInt()>255
        || regIP.cap(4).toInt()>255 )
        QMessageBox::warning(this, "Login - Qlic2Qall", tr("Bad IP adress"));
    else
        socket->connectToHost(ip, port);
        //telnetConnected();
}

