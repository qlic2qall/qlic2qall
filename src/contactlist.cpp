#include "contactlist.h"

#include "ldap.h"

#include <QMessageBox>

/*******************************************************/
/*                    Contact CLASS                    */
/*******************************************************/
int Contact::counter = 0;

// Suppose that the number is valid ...
QString Contact::formatNumber(const QString &_number)
{
    QString num = _number;
    QString numCleaned;

    if (num.startsWith("+"))
        num.replace(0, 1,"00");
    num.remove('+');
    num.remove('-');
    for(int i = 0; i < num.length(); ++i)
        if (num.at(i).isDigit())
            numCleaned += num.at(i);

    if (!numCleaned.isEmpty() && numCleaned.at(0) == '0' && numCleaned.at(1) != '0')
        numCleaned.replace(0,1, NATIONAL_PREFIXE);

    return numCleaned;
}

Contact::Contact(const QStringList &SL) : id(counter++), stringList(SL)
{
    while (stringList.size() < 8)
        stringList << "";

    // Format all numbers to correct numbers (need to be as much standard as possible : reading event)
    if (!stringList.at(HOME).isEmpty()) stringList.replace(HOME, formatNumber(stringList.at(HOME)));
    if (!stringList.at(WORK).isEmpty()) stringList.replace(WORK, formatNumber(stringList.at(WORK)));
    if (!stringList.at(MOBILE).isEmpty()) stringList.replace(MOBILE, formatNumber(stringList.at(MOBILE)));
    if (!stringList.at(FAX).isEmpty()) stringList.replace(FAX, formatNumber(stringList.at(FAX)));

    // Build the display name
    displayName = stringList.value(NAME) + " " + stringList.value(SURNAME);
    if (!stringList.value(DETAILS).isEmpty())
        displayName += QString (" [%1]").arg(stringList.value(DETAILS));
    displayName = displayName.simplified();

    // Set the user icon
    icon = QIcon("./images/identity.png");

    // Build the list item that represent this contact
    item = new QListWidgetItem(icon, displayName);
    item->setSizeHint(QSize(item->sizeHint().width(), LIST_ITEM_HEIGHT));

    // Build list items that represent numbers of this contact
    QString str;
    QListWidgetItem *tmp;
    if (!(str=stringList.value(HOME)).isEmpty())
    {
        tmp = new QListWidgetItem(QIcon("./images/gohome.png"), str);
        tmp->setToolTip(QString("Home : ") + str);
        tmp->setSizeHint(QSize(item->sizeHint().width(), LIST_ITEM_HEIGHT));
        numbers.append(tmp);
    }
    if (!(str=stringList.value(WORK)).isEmpty())
    {
        tmp = new QListWidgetItem(QIcon("./images/kservices.png"), str);
        tmp->setToolTip(QString("Work : ") + str);
        tmp->setSizeHint(QSize(item->sizeHint().width(), LIST_ITEM_HEIGHT));
        numbers.append(tmp);
    }
    if (!(str=stringList.value(MOBILE)).isEmpty())
    {
        tmp = new QListWidgetItem(QIcon("./images/sms.png"), str);
        tmp->setToolTip(QString("Mobile : ") + str);
        tmp->setSizeHint(QSize(item->sizeHint().width(), LIST_ITEM_HEIGHT));
        numbers.append(tmp);
    }
    if (!(str=stringList.value(FAX)).isEmpty())
    {
        tmp = new QListWidgetItem(QIcon("./images/kfax.png"), str);
        tmp->setToolTip(QString("Fax : ") + str);
        tmp->setSizeHint(QSize(item->sizeHint().width(), LIST_ITEM_HEIGHT));
        numbers.append(tmp);
    }
    if (!(str=stringList.value(INTERNAL)).isEmpty())
    {
        tmp = new QListWidgetItem(QIcon("./images/network.png"), str);
        tmp->setToolTip(QString("Internal : ") + str);
        tmp->setSizeHint(QSize(item->sizeHint().width(), LIST_ITEM_HEIGHT));
        numbers.append(tmp);
    }
}

Contact::~Contact()
{
    delete item;
    while (!numbers.isEmpty())
        delete numbers.takeFirst();
}

int Contact::getId() const
{
    return id;
}

const QStringList & Contact::getSL() const
{
    return stringList;
}

const QString & Contact::getDisplayName() const
{
    return displayName;
}

void Contact::setDisplayName(const QString &name)
{
    displayName = name;
}

QListWidgetItem * Contact::getItem() const
{
    return item;
}

const QList<QListWidgetItem *> & Contact::getNumberItems() const
{
    return numbers;
}

/*******************************************************/
/*                  ContactList CLASS                  */
/*******************************************************/
ContactList::ContactList(QWidget *parent) : QWidget(parent)
{
    // Initializing state and widgets
    state = selectingUser;
    searchLE = new QLineEdit(this);
    contactList = new QListWidget(this);
    callPB = new QPushButton(QIcon("./images/redo.png"), "", this);

    // Updating the style and reactions of the contactList
    searchLE->installEventFilter(this);
    QPalette p = QApplication::palette();
    p.setColor(QPalette::Inactive, QPalette::Highlight, p.color(QPalette::Active, QPalette::Highlight));
    p.setColor(QPalette::Inactive, QPalette::HighlightedText, p.color(QPalette::Active, QPalette::HighlightedText));
    contactList->setPalette(p);
    contactList->setFocusPolicy(Qt::NoFocus);
    contactList->setAlternatingRowColors(true);
    contactList->setSelectionMode(QAbstractItemView::SingleSelection);
    callPB->setFocusPolicy(Qt::NoFocus);
    callPB->setToolTip(trUtf8("Initiate a call"));


    connect(searchLE, SIGNAL(textEdited(QString)), this, SLOT(filterList(QString)));    // See the difference between textEdited() and textChanged()
    connect(contactList, SIGNAL(itemActivated(QListWidgetItem *)), this, SLOT(contactSelected(QListWidgetItem *)));
    connect(callPB, SIGNAL(clicked()), this, SLOT(callPB_clicked()));

    // Setting the layout of the QWidget
    QVBoxLayout *vbox = new QVBoxLayout();
    QHBoxLayout *hbox = new QHBoxLayout();
    vbox->setContentsMargins(0,0,0,0);
    hbox->setContentsMargins(0,0,0,0);
    hbox->addWidget(searchLE);
    hbox->addWidget(callPB);
    vbox->addLayout(hbox);
    vbox->addWidget(contactList);
    this->setLayout(vbox);
}

ContactList::~ContactList()
{
    while (!contacts.isEmpty())
        delete contacts.takeFirst();
}

bool ContactList::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == searchLE && event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        switch (keyEvent->key())
        {
            case Qt::Key_Up     :
            case Qt::Key_Down   :
                QApplication::postEvent(contactList, new QKeyEvent(*keyEvent));
                return true;    // Event is filtered and don't go to the watched Object (searchLE)
                break;
            case Qt::Key_Return :
            case Qt::Key_Enter  :
                if (contactList->count() > 0)
                    QApplication::postEvent(contactList, new QKeyEvent(*keyEvent));
                else
                    callPB_clicked();
                return true;    // Event is filtered and don't go to the watched Object (searchLE)
                break;
            default:
                return QWidget::eventFilter(watched, event);
        }
    }

    return QWidget::eventFilter(watched, event);
}
/***
 *  Load contacts from a CSV file
 */
bool ContactList::loadContacts(const QString &path)
{
    QFile file;
    QTextStream in;
    QString tmp;

    file.setFileName(path);

    if (file.exists()) {
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            in.setDevice(&file);

            // Read the first line (column titles)
            tmp = in.readLine();

            while (!in.atEnd()) {
                tmp = in.readLine();
                if (tmp.isEmpty()) continue;
                contacts.append(new Contact(tmp.split(QChar(';'), QString::KeepEmptyParts)));
            }
            file.close();

            // Populate the list
            filterList("");
        }
        else {
            QMessageBox::warning(this, trUtf8("Loading contacts from CSV file - Qlic2Qall"), trUtf8("The file can't be opened"));
            return false;
        }
    }
    else {
        QMessageBox::warning(this, trUtf8("Loading contacts from CSV file - Qlic2Qall"), trUtf8("The file specified doesn't exist"));
        return false;
    }
    return true;
}

/***
 *  Load contacts from a LDAP server
 */
bool ContactList::loadContacts(const QString &address, const QString &base)
{
    LDAP *ld;
    int  result;

    if (address.isEmpty() || base.isEmpty())
    {
        QMessageBox::warning(this, trUtf8("Loading LDAP contacts - Qlic2Qall"), trUtf8("Address or base parameter is empty"));
        return false;
    }

   /***
    *   Initializing the LDAP server connection structure
    */
    result = ldap_initialize(&ld, address.toLatin1().constData());
    if (result != LDAP_SUCCESS) {
        QMessageBox::warning(this, trUtf8("Loading LDAP contacts - Qlic2Qall"), trUtf8("Problem when initializing LDAP structure :\nldap_initialize: %1").arg(ldap_err2string(result)));
        return false;
    }

   /***
    *   Set the protocol version to 3
    */
    int desired_version = LDAP_VERSION3;
    result = ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &desired_version);
    if (result != LDAP_OPT_SUCCESS) {
        QMessageBox::warning(this, trUtf8("Loading LDAP contacts - Qlic2Qall"), trUtf8("Problem when setting LDAP protocol version to v3"));
        return false;
    }

//   /***
//    *   Set a timeout for synchronous call
//    */
//    struct timeval tv;
//    tv.tv_sec = 5;
//    tv.tv_usec = 0;
//    result = ldap_set_option(ld, LDAP_OPT_TIMEOUT, &tv);
//    if (result != LDAP_OPT_SUCCESS) {
//        fprintf(stderr, "ldap_set_option: failed!\n");
//        exit( EXIT_FAILURE );
//    }


    // TODO start a waiting bar (it can take many times if the connection is not OK)

   /***
    *   Fetch all records (should I filter on something?)
    */
    //char *attrs[] = {LDAP_NO_ATTRS, NULL};    // No attributes
    LDAPMessage *resultMessage;
    result = ldap_search_ext_s(ld,
                               qPrintable(base),
                               LDAP_SCOPE_ONELEVEL,
                               NULL, // NULL = "(objectClass=*)",
                               NULL, // attrs,
                               0,
                               NULL,
                               NULL,
                               NULL,
                               0,
                               &resultMessage);
    // TODO stop the waiting bar
    if (result != LDAP_SUCCESS) {
        QMessageBox::warning(this, trUtf8("Loading LDAP contacts - Qlic2Qall"), trUtf8("Problem when searching in the LDAP server :\nldap_search_ext_s: %1").arg(ldap_err2string(result)));
        return false;
    }

   /***
    *   Create a new contact for each result (Prénom;Nom;Detail;Home;work;Mobile;FAX;Internal)
    */
    QStringList newContact;
    struct berval **values;
    LDAPMessage *entry;
    for (entry = ldap_first_entry(ld, resultMessage); entry != NULL; entry = ldap_next_entry(ld, entry))
    {
        newContact.clear();

        // Fetch name
        values = ldap_get_values_len(ld, entry, "givenName");
        if (ldap_count_values_len(values) > 0) newContact << QString::fromUtf8(values[0]->bv_val);
        else                                   newContact << QString();
        ldap_value_free_len(values);

        values = ldap_get_values_len(ld, entry, "sn");
        if (ldap_count_values_len(values) > 0) newContact << QString::fromUtf8(values[0]->bv_val);
        else                                   newContact << QString();
        ldap_value_free_len(values);

        values = ldap_get_values_len(ld, entry, "detail");
        if (ldap_count_values_len(values) > 0) newContact << QString::fromUtf8(values[0]->bv_val);
        else                                   newContact << QString();
        ldap_value_free_len(values);

        values = ldap_get_values_len(ld, entry, "homePhone");
        if (ldap_count_values_len(values) > 0) newContact << QString::fromUtf8(values[0]->bv_val);
        else                                   newContact << QString();
        ldap_value_free_len(values);

        values = ldap_get_values_len(ld, entry, "telephoneNumber");
        if (ldap_count_values_len(values) > 0) newContact << QString::fromUtf8(values[0]->bv_val);
        else                                   newContact << QString();
        ldap_value_free_len(values);

        values = ldap_get_values_len(ld, entry, "mobile");
        if (ldap_count_values_len(values) > 0) newContact << QString::fromUtf8(values[0]->bv_val);
        else                                   newContact << QString();
        ldap_value_free_len(values);

        values = ldap_get_values_len(ld, entry, "facsimileTelephoneNumber");
        if (ldap_count_values_len(values) > 0) newContact << QString::fromUtf8(values[0]->bv_val);
        else                                   newContact << QString();
        ldap_value_free_len(values);

        values = ldap_get_values_len(ld, entry, "employeeNumber");
        if (ldap_count_values_len(values) > 0) newContact << QString::fromUtf8(values[0]->bv_val);
        else                                   newContact << QString();
        ldap_value_free_len(values);

        contacts.append(new Contact(newContact));
    }

    ldap_msgfree(resultMessage);

    // Populate the list
    filterList("");

    return true;
}

Contact * ContactList::findContact(QListWidgetItem * const item) const
{
    for (int i = 0; i < contacts.size(); ++i)
        if (contacts.at(i)->getItem() == item)
            return contacts.at(i);

    return NULL;
}

bool ContactList::isValidNumber(QString num)
{
    int count = 0;

    num = num.simplified();
    count = num.count(QChar('+'));
    num = num.remove(QChar('+'));
    num = num.remove(QChar(' '));

    if (!num.isEmpty() && count <= 1)
    {
        QRegExp exp("[0-9]+");
        if (exp.exactMatch(num))
            return true;
    }

    return false;
}

void ContactList::filterList(const QString &text)
{
    QString contact;
    QStringList texts = text.simplified().split(QChar(' '));
    bool match;

    state = selectingUser;

    // Clear the lists (Careful!! QListWidget::clear() also unallocate all QListWidgetItem* linked to it)
    while (contactList->count() > 0)
        contactList->takeItem(0);

    // For each contact in the list
    for (int i = 0; i < contacts.size(); ++i)
    {
        match = true;
        contact = contacts.at(i)->getSL().join(" ").simplified();

        // For each word in the searchLE
        for (int j = 0; j < texts.size(); ++j)
            if (!contact.contains(texts.at(j), Qt::CaseInsensitive))
                match = false;

        if (match)
            contactList->addItem(contacts.at(i)->getItem());
    }
    contactList->sortItems(Qt::AscendingOrder);

    if (contactList->item(0))
        contactList->setCurrentRow(0);
}

void ContactList::contactSelected(QListWidgetItem* c)
{
    Contact *currentContact = NULL;

    if (state == selectingUser && (currentContact=findContact(c)))
    {
        qDebug("User selected : %s %d",currentContact->getDisplayName().toLatin1().data(), currentContact->getId());
        emit userSeleced(currentContact->getDisplayName());
        state = selectingNumber;

        searchLE->setText(currentContact->getDisplayName());

        // Clear the list without deleting items
        while (contactList->count() > 0)
            contactList->takeItem(0);

        for (int i = 0; i < currentContact->getNumberItems().size(); ++i)
            contactList->addItem(currentContact->getNumberItems().at(i));

        if (contactList->item(0))
            contactList->setCurrentRow(0); // TODO : update the text of the searchLE (add the number in parenthesis or something like that)

        searchLE->selectAll();
    }
    else if (state == selectingNumber)
    {
        qDebug("Number selected : %s", c->text().toLatin1().data());
        emit numberSelected(c->text()); // TODO , access the number stocked in the SL because the display of the number in the listItem can change...
        callPB_clicked();
    }
}

void ContactList::callPB_clicked()
{
    QString number;

    qDebug("callPB was clicked");

    searchLE->selectAll();
    searchLE->setFocus();

    if (state == selectingNumber && contactList->currentItem())
        number = contactList->currentItem()->text();
    else if (state == selectingNumber && !contactList->currentItem()) {
        QMessageBox::warning(this, trUtf8("Calling... - Qlic2Qall"), trUtf8("No number available for this contact"));
        return;
    } else
        number = Contact::formatNumber(searchLE->text());

    if (!isValidNumber(number))
    {
        QMessageBox::warning(this, trUtf8("Calling... - Qlic2Qall"), trUtf8("The number entered/selected is invalid"));
        return;
    }

    emit callAsked(number.remove(QChar(' ')));
}

QString ContactList::nameFromNumber(const QString &number) const
{
    QString ret = QString("");

    QList<int> numberColumns;
    numberColumns << HOME << WORK << MOBILE << FAX << INTERNAL;    // Add all collumn which contains a phone number

    for (int j = 0; j < contacts.size(); ++j)
    {
        for (int i = 0; i < numberColumns.size(); ++i)
        {
            QString tmp = contacts.at(j)->getSL().value(numberColumns.at(i));

            if (!tmp.isEmpty() && tmp == Contact::formatNumber(number))
                return contacts.at(j)->getDisplayName();
        }
    }

    return ret;
}

void ContactList::clearContacts()
{
    // Clear contact list
    while (!contacts.isEmpty())
        delete contacts.takeFirst();

    // Clear search line edit
    searchLE->clear();
}
