#include "qtfilechooser.h"

#include <QApplication>
#include <QEvent>
#include <QFileDialog>
#include <QFile>
#include <QLayout>
#include <QLineEdit>
#include <QPushButton>

QtFileChooser::QtFileChooser(QWidget *parent)
    :QWidget(parent)
    ,m_lineEdit(new QLineEdit(this))
    ,m_pushButton(new QPushButton("...", this))
    ,m_openPath(QDir::currentPath())
    ,m_openFolder(false)
{
    m_lineEdit->installEventFilter(this);
    m_pushButton->setMaximumHeight(22);
    m_pushButton->setMaximumWidth(22);

    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->setContentsMargins(0, 0, 0, 0);
    hbox->setSpacing(2);
    hbox->addWidget(m_lineEdit);
    hbox->addWidget(m_pushButton);
    setLayout(hbox);

    m_pushButton->setFocus();

    connect(m_pushButton, SIGNAL(clicked()), this, SLOT(slotChooseFile()));
    connect(m_lineEdit, SIGNAL(textChanged(const QString &)), this, SLOT(updateBackgroundColor()));
}

QtFileChooser::~QtFileChooser()
{
}

QString QtFileChooser::text() const
{
    return m_lineEdit->text();
}

void QtFileChooser::setText(const QString &text)
{
    m_lineEdit->setText(text);
}

QString QtFileChooser::fileFilter() const
{
    return m_fileFilter;
}
void QtFileChooser::setFileFilter(const QString &fileFilter)
{
    m_fileFilter = fileFilter;
}

bool QtFileChooser::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == m_lineEdit && event->type() == QEvent::FocusOut)
        updateBackgroundColor();

    return QWidget::eventFilter(obj, event);
}

void QtFileChooser::slotChooseFile()
{
    QString path;
    const QString openDir = QFile::exists(m_lineEdit->text())?m_lineEdit->text():QDir::currentPath();

    if (!m_openFolder)
        path = QFileDialog::getOpenFileName(this, trUtf8("Choose a file"), openDir, m_fileFilter);
    else
        path = QFileDialog::getExistingDirectory(this, trUtf8("Choose a directory"), openDir);

    if (!path.isEmpty()) {
        m_lineEdit->setText(path);
        updateBackgroundColor();
    }
}

void QtFileChooser::updateBackgroundColor()
{
    const QString path = m_lineEdit->text();
    QPalette p = m_lineEdit->palette();

    if (path.isEmpty())
        p.setColor(QPalette::Active, QPalette::Base, qApp->palette().color(QPalette::Base));
    else {
        if (QFile::exists(path))
            //p.setColor(QPalette::Active, QPalette::Base, QColor(0,255,0).light());
            p.setColor(QPalette::Base, qApp->palette().color(QPalette::Base));
        else
            p.setColor(QPalette::Active, QPalette::Base, QColor(255,0,0).light());
    }

    m_lineEdit->setPalette(p);
}
