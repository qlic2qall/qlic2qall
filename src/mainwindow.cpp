#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "contactlist.h"
#include "configurationdialog.h"

#include <QEvent>
#include <QMenu>
#include <QSettings>
#include <QTimer>
#include <QTcpSocket>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

   /***
    *   Add the contact list widget
    */
    m_contactList = new ContactList(ui->contactsW);
    QVBoxLayout *vbox = new QVBoxLayout(ui->contactsW);
    vbox->setContentsMargins(0,0,0,0);
    vbox->addWidget(m_contactList);

   /***
    *   Setup the tray icon
    */
    this->setWindowIcon(QIcon("./images/linphone.png"));
    tray = new QSystemTrayIcon(QIcon("./images/linphone.png"), this);
    tray->setVisible(true);
    trayMenu = new QMenu(this);
    buildTrayMenu();


   /***
    *   Load last geometry and pos
    */
    QSettings settings;
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());

   /***
    *   Load list of contacts
    */
    loadContacts();

    connect(m_contactList, SIGNAL(callAsked(QString)), this, SLOT(initiateCall(QString)));
    connect(ui->actionReload, SIGNAL(triggered()), this, SLOT(loadContacts()));
    connect(ui->actionConfigure, SIGNAL(triggered()), this, SLOT(slotConfigureDialog()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QSettings settings;

   /***
    *   Save geometry and states (dock, toolbar)
    */
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());

    QMainWindow::closeEvent(event);
}

void MainWindow::buildTrayMenu()
{
    QAction *act;
    act = trayMenu->addAction(trUtf8("Show/hide"),this, SLOT(showHide()));
    trayMenu->addSeparator();
    trayMenu->addAction(trUtf8("&Quit"), this, SLOT(close()));
    trayMenu->setDefaultAction(act);
    connect(tray, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayActivated(QSystemTrayIcon::ActivationReason)));

    tray->setContextMenu(trayMenu);
}

void MainWindow::trayActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
        case QSystemTrayIcon::DoubleClick:
        //case QSystemTrayIcon::Trigger:
            showHide();
            break;
        default:
            break;
    }
}

void MainWindow::showHide()
{
    this->setVisible(!this->isVisible());

    if (this->isVisible())
    {
        this->show();
        this->raise();
        this->activateWindow();
    }
}

void MainWindow::setSocket(QTcpSocket *sock)
{
    socket = sock;
    // TODO check if link is up
    connectionEnable = true;

    connect(socket, SIGNAL(readyRead()), this, SLOT(treatIncomingData()));
}

void MainWindow::treatIncomingData()
{
    /*
        Event: Newchannel
        Privilege: call,all
        Channel: SIP/85.119.188.3-08416c60
        ChannelState: 0
        ChannelStateDesc: Down
        CallerIDNum: 080510275
        CallerIDName: 080510275
        AccountCode:
        Uniqueid: 1270463405.88
    */

    while (socket->canReadLine())
    {
        QString s = socket->readLine();
		QString callerIDname;
        s.remove('\n'); s.remove('\r');
        if (!s.isEmpty())
        {
            //qDebug("%s", s.toLatin1().data());
            if (s.contains("Event: Newchannel"))
            {
                socket->readLine(); // Privilege:
                s = socket->readLine(); // Channel:
                //if (s.contains("patton") || s.contains("SIP/5060") || s.contains("SIP/10.0.0.49"))
                if (s.contains("SIP/85.119.188.3"))
                {
                    socket->readLine(); // ChannelState:
                    socket->readLine(); // ChannelStateDesc:
                    s = socket->readLine(); // CallerIDNum:
					callerIDname = socket->readLine(); // CallerIDName
                    if (!callerID.contains(s)) {
                        callerID.enqueue(s);
                        QTimer::singleShot(3000, this, SLOT(timerSlot()));
                    }
                    else {
                        continue;
                    }

                    QStringList tmp = s.split(':');
                    if (tmp.size() == 2)
                    {
                        s = tmp.at(1).simplified();
						callerIDname = callerIDname.section(":",1).simplified();

//                        s.remove(0,1); // Remove the prefixed 0 that come from the "Forum i4 NET"
                        if (!s.isEmpty())
                        {
                            QString name = m_contactList->nameFromNumber(s);
                            if (name.isEmpty())
							{
								if (!callerIDname.isEmpty() && callerIDname != "UNKNOWN" && callerIDname != s)
									name = callerIDname;
								else
									name = trUtf8("Unknown number");
							}
                            tray->showMessage(trUtf8("Qlic2Qall : incoming call"),QString("%1 : %2").arg(name).arg(s), QSystemTrayIcon::Information, 5000);
                        }
                    }
                }
            }
        }
    }
}

void MainWindow::initiateCall(QString number)
{
    if (connectionEnable)
    {
        QSettings settings;

        qDebug("Initiating call to %s", number.toLatin1().data());

        socket->write("Action: Originate\r\n");
        socket->write(QString("Channel: SIP/%1\r\n").arg(settings.value("login/username").toString()).toLatin1().data());
        socket->write("Context: internal\r\n");
        socket->write(QString("Exten: %1\r\n").arg(number).toLatin1().data());
        socket->write("Priority: 1\r\n");
        socket->write(QString("Callerid: %1   [Qlic2Qall]\r\n").arg(number).toLatin1().data());
        socket->write("Timeout: 30000\r\n");
        socket->write("\r\n");
    }

    // TODO, check if write was fine
}

void MainWindow::timerSlot()
{
    callerID.dequeue();
}

void MainWindow::slotConfigureDialog()
{
    ConfigurationDialog dialog(this);
    if (dialog.exec() == QDialog::Accepted)
        loadContacts();
}

void MainWindow::loadContacts()
{
    QSettings settings;

    m_contactList->clearContacts();

    if (settings.value("config/useCSV", false).toBool())
        m_contactList->loadContacts(settings.value("config/CSVfilePath", "").toString());
    if (settings.value("config/useLDAP", false).toBool())
        m_contactList->loadContacts(settings.value("config/LDAPaddress", "").toString(), settings.value("config/LDAPbase", "").toString());
}
